import { Comment } from '@services';
export type CommentResponse = { comment: Comment };
