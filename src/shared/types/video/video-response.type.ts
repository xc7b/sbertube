import { Video } from '@services';

export type VideoResponse = {
	video: Video;
};
